import ReactDOM from 'react-dom';
import React, {useState, useEffect} from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import Pagination from "react-bootstrap/Pagination";

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Bar } from 'react-chartjs-2';
 

  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );
  
  export const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' ,
      },
      title: {
        display: true,
        text: 'Chart.js Bar Chart',
      },
    },
  };
  
  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  
  const _data = {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: [1, 2, 3, 4, 5, 6, 7],
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: [1, 2, 3, 4, 5, 6, 7],
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
      {
        label: 'Dataset 3',
        data: [1, 2, 3, 4, 5, 6, 7],
        backgroundColor: 'rgba(53, 162, 23, 0.5)',
      },
    ],
  };
  


function PaginationWithElasticSearch() {
    const [data, setData] = useState([]);
    const [count, setCount] = useState(0);
    const [pageNo, setPageNo] = useState(1);
    const [show, setShow] = useState(false);
    

    // clear
    // localStorage.removeItem("latestPage");
    // localStorage.removeItem("show");

    const id = parseInt(window.localStorage.getItem('latestPage')) || pageNo
    const showPageBucket = JSON.parse(window.localStorage.getItem('show')) || show

    let arr = []
    const handleSetPage = (pageNo) => {
        setPageNo(pageNo);
        window.localStorage.setItem('latestPage', pageNo)
    }
    
    const handleFirstPage = () => {
        setPageNo(1);
        window.localStorage.setItem('latestPage', JSON.stringify(1));
    }

    useEffect(() => {

        //const url = `http://localhost:5001/api/es/${id}`; // nodejs
        // const url = `http://localhost:8000/api/es/${id}`;    // laravel
        const url = `http://localhost:8000/polls/api`;    // python
        axios.get(url)
            .then(result => {
                console.log('result--->', result)
                setCount(result.data.count);
                setData(result.data.data);
            })
            .catch(err => console.log(err))
    }, [id])
    console.log('data----->', data)
    for (let i = 1; i <= Math.ceil(count / 10); i++)
        arr.push(i);

    arr = arr.map(val => <Pagination.Item key={val} active={val === id}
                                          onClick={() => handleSetPage(val)}>{val}</Pagination.Item>)
    return (
        <div>
            <Bar options={options} data={_data} />
            <Table striped bordered hover style={{textAlign: "center"}}>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Timestamp</th>
                    <th>Device</th>
                    <th>Src</th> 
                    <th>Dst</th> 
                </tr>
                </thead>
                <tbody>
                {data.map((doc, ind) => {
                    return (
                        <tr key={ind}>
                            <td>{(id - 1) * 10 + ind + 1}</td>
                            <td>{doc._source.syslog_timestamp}</td>
                            <td>{doc._source.device}</td>
                            <td>{doc._source.src_ip}</td>
                            <td>{doc._source.dst_ip}</td>
                        </tr>
                    )
                })}
                </tbody>
            </Table>
            <Pagination className={"no-wrap"}>
                <Pagination.First onClick={() => handleFirstPage()}/>
                <Pagination.Prev onClick={() => {
                    if (pageNo !== 1) {
                        setPageNo(pageNo - 1)
                        window.localStorage.setItem('latestPage', JSON.stringify(pageNo - 1))
                    } else {
                        setPageNo(pageNo);
                        window.localStorage.setItem('latestPage', JSON.stringify(pageNo));
                    }
                }}/>
                {showPageBucket ? arr : [arr[id - 1], arr[id], arr[id + 1], arr[id + 2], arr[id + 3], arr[id + 4], arr[id + 5], arr[id + 6], arr[id + 7], arr[id + 8], arr[id + 9]]}

                <Pagination.Next
                    onClick={() => {
                        if (pageNo !== Math.ceil(count / 10)) {
                            setPageNo(pageNo + 1);
                            window.localStorage.setItem('latestPage', JSON.stringify(pageNo + 1))
                        } else {
                            setPageNo(pageNo);
                            window.localStorage.setItem('latestPage', JSON.stringify(pageNo))
                        }
                    }}/>
                <Pagination.Last onClick={() => {
                    setPageNo(Math.ceil(count / 10));
                    window.localStorage.setItem('latestPage', JSON.stringify(Math.ceil(count / 10)))
                }}/>
            </Pagination>
        </div>
    )
}

// export default PaginationWithElasticSearch;



ReactDOM.render(<PaginationWithElasticSearch />, document.getElementById('react-app'));