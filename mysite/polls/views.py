from itertools import count
from django.http import HttpResponse, JsonResponse
from django.template import loader

from elasticsearch import Elasticsearch
# from .models import Question
from pprint import pprint


def index(request):
    # TODO
    #  client = Elasticsearch('http://192.168.1.72:9200', http_auth=('elastic', 'isylzjkoD3v'))\
    #  result_search = client.search(index='logstash-syslog-*', filter_path=['hits.hits.*'])

    # latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'data': [],
    }
    return HttpResponse(template.render(context, request))

def api_search1(request):

    query_body = {
        "query": {
            "match": {
                "src_ip": "192.168.1.72"
            }
        }
    }
    index2 = 'logstash-syslog-2022.03.30'
    client2 = Elasticsearch('http://192.168.1.72:9200', http_auth=('elastic', 'isylzjkoD3v'))
    result_search2 = client2.search(index=index2, body=query_body,  filter_path=['hits.hits.*'])
    result_count2 = client2.count(index=index2, body=query_body)
    ret = {
        "count" : result_count2['count'],
        "data" : result_search2['hits']['hits']
    }
    return ret

def api_search2(request):

    query_body = {
        "query": {
            "match": {
                "src_ip": "192.168.1.72"
            }
        }
    }
    index2 = 'logstash-syslog-2022.03.31'
    client2 = Elasticsearch('http://192.168.1.72:9200', http_auth=('elastic', 'isylzjkoD3v'))
    result_search2 = client2.search(index=index2, body=query_body,  filter_path=['hits.hits.*'])
    result_count2 = client2.count(index=index2, body=query_body)
    ret = {
        "count" : result_count2['count'],
        "data" : result_search2['hits']['hits']
    }
    return ret


def api(request):
    result_search1 = api_search1(request)
    # result_search2 = api_search2(request)
    # toto_count = result_search1.get('count') + result_search2.get('count')
    list1 = result_search1.get('data')
    # list2 = result_search2.get('data')

    # for i in list2:
    #     list1.append(i)

    # total_count = result_search1.get('count') + result_search2.get('count')

    total_count = result_search1.get('count')

    # pprint(len(list1))

    json = {
        "count": total_count,
        "data": list1
    }
    return JsonResponse(json)